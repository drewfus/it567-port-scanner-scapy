def parseTargetLine(targetLine):
	if "/" in targetLine:
		#has subnet
		base = targetLine.split("/")[0]
		baseSplit = [int(base.split(".")[0]), int(base.split(".")[1]), int(base.split(".")[2]), int(base.split(".")[3])]
		slash = targetLine.split("/")[1]
		count = 2**(32-int(slash))
		#print("base: " + base)
		#print("slash: " + slash)
		#print("count: " + str(count))
		
		targets = []
		for a in range(baseSplit[0], 256):
			for b in range(baseSplit[1], 256):
				for c in range(baseSplit[2], 256):
					for d in range(baseSplit[3], 256):
						if count == 0:
							print("targets: " + str(targets))
							return targets
						targets.append(str(a) + "." + str(b) + "." + str(c) + "." + str(d))
						count = count-1
					baseSplit[3] = 0
				baseSplit[2] = 0
			baseSplit[1] = 0

		return [targetLine]
	
	elif "-" in targetLine:
		#has range
		first = targetLine.split("-")[0]
		firstSplit = int(first.split(".")[0]), int(first.split(".")[1]), int(first.split(".")[2]), int(first.split(".")[3])
		last = targetLine.split("-")[1]
		lastSplit = int(last.split(".")[0]), int(last.split(".")[1]), int(last.split(".")[2]), int(last.split(".")[3])
		targets = []
		for a in range(firstSplit[0], lastSplit[0]+1):
			for b in range(firstSplit[1], lastSplit[1]+1):
				for c in range(firstSplit[2], lastSplit[2]+1):
					for d in range(firstSplit[3], lastSplit[3]+1):
						targets.append(str(a) + "." + str(b) + "." + str(c) + "." + str(d))
		print("targets: " + str(targets))
		return targets

	else:
		print("target: " + targetLine)
		return [targetLine]


def parsePortLine(portLine):
	separateByComma = portLine.split(",")
	ports = []
	for group in separateByComma:
		if "-" in group:
			for i in range(int(group.split("-")[0]), int(group.split("-")[1])+1):
				ports.append(i)
		else:
			ports.append(int(group))
	print("ports: " + str(ports))
	return ports


def parseProtocolLine(protocolLine):
	protocols = []
	if "tcp" in protocolLine.lower():
		protocols.append("TCP")
	if "udp" in protocolLine.lower():
		protocols.append("UDP")
	if "icmp" in protocolLine.lower():
		protocols.append("ICMP")
	print("protocols: " + str(protocols))
	return protocols
