import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)
from scapy.all import *
import sys, getopt
from parse import *


def main():
	try:
		opts, args = getopt.getopt(sys.argv[1:], "ht:p:", ["help", "target=", "port=", "protocol="])
	except getopt.GetoptError as err:
		# print help information and exit:
		print(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)

	targetLine = None
	portLine = None
	protocolLine = None

	for o, a in opts:
		if o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-t", "--target"):
			#print("target entered is: " + a)
			targetLine = a
		elif o in ("-p", "--port"):
			#print("port entered is: " + a)
			portLine = a
		elif o in ("--protocol"):
			#print("protocol entered is: " + a)
			protocolLine = a
		#assert False, "unhandled option"

	if len(opts) == 0:
		print("ERROR: No target specified - target required")
		usage()
		sys.exit()
	if not portLine:
		print("no port specified, scanning 10 most common ports"
					"\n\t(21-23,25,80,110,139,443,445,3389)")
		portLine = "21-23,25,80,110,139,443,445,3389"
	if not protocolLine:
		print("no protocol specified, using TCP")
		protocolLine = "TCP"

	manageScan(targetLine, portLine, protocolLine)
	#example()

def usage():
	print("\noptions:"
				"\n\t-t (target) (use dash for range, slash for subnet)"
				"\n\t-p (port) (separated by comma's and/or dashes)"
				"\n\t--protocol (TCP,UDP,ICMP)"

				"\n\n\texample: -t 192.168.207.0/24 -p 1-1024,3389 --protocol TCP,UDP"
				"\n\texample: -t 192.168.207.118-192.168.207.126 -p 22,139 --protocol TCP")


def manageScan(targetLine, portLine, protocolLine):
	#print("parsing")
	targets = parseTargetLine(targetLine)
	ports = parsePortLine(portLine)
	protocols = parseProtocolLine(protocolLine)

	print("scanning...")
	for target in targets:
		for protocol in protocols:
			scan(target, ports, protocol)


def scan(target, ports, protocol):
	body = None
	if "TCP" in protocol:
		body = TCP(dport = ports, flags="S")
		ans, unans = sr(IP(dst = target)/body, timeout=2)
		reportTCP(target, ans)
	if "UDP" in protocol:
		body = UDP(dport = ports)
		print("will wait up to 20 seconds...")
		ans, unans = sr(IP(dst = target)/body, timeout=20)
		reportUDP(target, ans)
	if "ICMP" in protocol:
		body = ICMP()
		ans = sr1(IP(dst = target)/body, timeout=2)
		reportICMP(target, ans)

def reportTCP(target, answer):
	print("Report for TCP scan of " + target)
	print("\tPort\tResponse\tService")
	for snd,rcv in answer:
		print("\t" + str(rcv.payload.sport) + "\t" + rcv.sprintf("%TCP.flags%") + openTCP(rcv.payload.flags) + rcv.sprintf("\t%TCP.sport%"))

def reportICMP(target, answer):
	print("Report for ICMP scan of " + target)
	if answer:
		print("\t host is alive")
			
			
def reportUDP(target, answer):
	print("Report for TCP scan of " + target)
	print(str(len(answer)) + " answered")
	print("\tPort\tResponse")
	for snd,rcv in answer:
		print("\t" + str(rcv.payload.sport) + "\tPort open")


def openTCP(flag):
	if flag == 18:
		return " (open)"
	else:
		return "\t"


if __name__ == "__main__":
	main()
